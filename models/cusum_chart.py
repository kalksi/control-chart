
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("simulation"))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("Configurations"))))
import math
from utils import plotUtils
import numpy as np

plotUtils.figsize=18
n = 28
sigma = 4
mean = 0
d = 0.5
B = 4
k = d*(sigma/math.sqrt(n))
H = B*(sigma/math.sqrt(n))

def cusumChart(subgroups):
    S_o = 0
    S_u = 0
    S_o_history = []
    S_u_history = []
    for subgroup in subgroups:
        S_o = max(S_o + (subgroup - mean - k), 0)
        S_o_history.append(S_o)
        S_u = min(S_u + (subgroup - mean + k), 0)
        S_u_history.append(S_u)
        #if(S_o > (mean + H) or S_u < (mean - H) ):
            #print("Der Prozess außer Kontrolle.")
    return S_o_history,S_u_history

def cusumChart_with_History(subgroups,S_o,S_u):
    S_o_history = []
    S_u_history = []
    for subgroup in subgroups:
        S_o = max(S_o + (subgroup - mean - k), 0)
        S_o_history.append(S_o)
        S_u = min(S_u + (subgroup - mean + k), 0)
        S_u_history.append(S_u)
        #if(S_o > (mean + H) or S_u < (mean - H) ):
            #print("Der Prozess außer Kontrolle.")
    return S_o_history,S_u_history


def get_So_Su(subgroups,S_o,S_u):
    S_o_history,S_u_history=cusumChart_with_History(subgroups,S_o,S_u)
    return S_o_history[-1],S_u_history[-1]

def print_cusum(subgroups):
    S_o_history, S_u_history = cusumChart(subgroups)
    plotUtils.plotCusumChart(subgroups,S_o_history,S_u_history,k,H,"")
    S_o_max= max(S_o_history)
    S_u_min= min(S_u_history)
    return S_o_max,S_u_min

def print_cusum_withHistory(subgroups,S_o,S_u):
    S_o_history, S_u_history = cusumChart_with_History(subgroups,S_o,S_u)
    plotUtils.plotCusumChart(subgroups,S_o_history,S_u_history,k,H,"")
    S_o_max= max(S_o_history)
    S_u_min= min(S_u_history)
    return S_o_max,S_u_min

def predict(subgroups):
    S_o_history, S_u_history = cusumChart(subgroups)
    S_o_max= max(S_o_history)
    S_u_min= min(S_u_history)
    up = True if (S_o_max > (mean + H)) else False
    down = True if (S_u_min < (mean - H)) else False
    return up,down

def predict_value(subgroups):
    S_o_history, S_u_history = cusumChart(subgroups)
    S_o_max= max(S_o_history)
    S_u_min= min(S_u_history)
    return S_o_max,S_u_min

def calculateAndAdd_So_Su(x,y):
    y_new=[]
    x_new=[]
    S_o,S_u= 0.0,0.0
    for i in range(len(x)):
       x_new.append(np.append(x[i], [S_o,S_u]))  
       S_o,S_u= get_So_Su(x[i],S_o,S_u)
       y_new.append(np.append(y[i], [S_o,S_u]))
    return np.asanyarray(x_new),np.asanyarray(y_new)

def calculate_So_Su(x):
    y_new=[]
    x_new=[]
    S_o,S_u= 0.0,0.0
    for i in range(len(x)):
       S_o,S_u= get_So_Su(x[i],S_o,S_u)
       y_new.append([S_o,S_u])
       x_new.append(np.append(x[i], [S_o,S_u]))  
    return np.asanyarray(x_new),np.asanyarray(y_new)

def calculate_withoutHistory_So_Su(x):
    y_new=[]
    x_new=[]
    S_o,S_u= 0.0,0.0
    for i in range(len(x)):
       S_o,S_u= get_So_Su(x[i],0.0,0.0)
       y_new.append([S_o,S_u])
       x_new.append(np.append(x[i], [S_o,S_u]))  
    return np.asanyarray(x_new),np.asanyarray(y_new)

def correctError(predictions):
    results=[]
    for i in range(len(predictions)):
        s_o= predictions[i][0] if(predictions[i][0]>=0) else 0
        s_u= predictions[i][1] if(predictions[i][1]<=0) else 0
        results.append([s_o,s_u])
    return results
