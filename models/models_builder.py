import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("Configurations"))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("simulation"))))
import Configurations as configs
from tensorflow import keras
from utils import modelUtils
from simulation import simulationProfile
from sklearn.ensemble import RandomForestRegressor
import joblib
from sklearn import svm

# get Training data
#neuralInputsNumber = configs.neuralInputsNumber
#targetVarNumber = configs.numberOftargetVariables
mainDir = ""
modelUtils.mainDir = mainDir
x, y, test_x, test_y = modelUtils.getTrainAndTestData()
layer_list_quad = [75, 89, 90]
layer_list_for_7x_m13 = [78, 87]
layer_list_for_7x_m08 =[95, 96]
layer_list_for_7x_m04 =[83, 72]

def get_ddn_layers():
    model = keras.Sequential([
    keras.layers.Input( shape=(configs.neuralInputsNumber),name="Eingabeschicht"),
    keras.layers.Dense(75, activation='relu',name="Verborgene_schicht_1"),
    keras.layers.Dense(75,  activation='relu',name="Verborgene_schicht_2"),
    keras.layers.Dense(configs.numberOftargetVariables, activation='sigmoid',name="Ausgabeschicht")])
    return model

def get_dnn_layers_for(layer_list):
    model = keras.Sequential([keras.layers.Input( shape=(configs.neuralInputsNumber),name="Eingabeschicht")])
    layernumber=1
    for l in layer_list:
        name="Verborgene_schicht_"+str(layernumber)
        model.add(keras.layers.Dense(l, activation='relu',name=name))
        layernumber=layernumber+1
    model.add(keras.layers.Dense(configs.numberOftargetVariables, activation='sigmoid',name="Ausgabeschicht"))
    return model

def get_ddn_layers_pred():
    model = keras.Sequential([
    keras.layers.Input( shape=(configs.neuralInputsNumber),name="Eingabeschicht"),
    keras.layers.Dense(65, activation='relu',name="Verborgene_schicht_1"),
    keras.layers.Dense(72,  activation='relu',name="Verborgene_schicht_2"),
    keras.layers.Dense(73,  activation='relu',name="Verborgene_schicht_3"),
    keras.layers.Dense(107,  activation='relu',name="Verborgene_schicht_4"),
    keras.layers.Dense(configs.numberOftargetVariables, activation='sigmoid',name="Ausgabeschicht")])
    return model

def dnn_model(model,name_suffix):
    model.compile(optimizer="adam",
              loss='binary_crossentropy',
              metrics=['accuracy'])
    history = model.fit(x, y, batch_size=16 ,epochs=30,validation_data=(test_x, test_y))
    predictions = model.predict(test_x)
    accurancy,confusionMatrix,report = modelUtils.getMetric(predictions,test_y)
    print("DNN Report")
    print(report)
    model.save(mainDir+configs.model_output_path+name_suffix+'/'+'dnn_model_'+name_suffix+'.h5')

def rf_model(name_suffix):
    model = RandomForestRegressor(n_estimators = 400, random_state = 42)
    model.fit(x, y)
    predictions = model.predict(test_x)
    accurancy,confusionMatrix,report = modelUtils.getMetric(predictions,test_y)
    print("RF Report")
    print(report)
    joblib.dump(model, mainDir+configs.model_output_path+name_suffix+'/random_forest_'+name_suffix+'.joblib')

def svm_model(name_suffix):
    model = svm.SVC()
    model.fit(x, modelUtils.transform_y_to_one_dimension(y))
    one_dimension_pred_y  = model.predict(test_x)
    predictions = modelUtils.transform_y_to_binary(one_dimension_pred_y )
    accurancy,_,report = modelUtils.getMetric(predictions,test_y)
    print("SVM Report")
    print(report)
    filename = mainDir+configs.model_output_path+name_suffix+'/svm_model_'+name_suffix+'.sav'
    joblib.dump(model, open(filename, 'wb'))

def upateDataSize():
    global x, test_x, neuralInputsNumber
    DataPoinstsNumber = 4
    x = x[:,0:DataPoinstsNumber]
    test_x = test_x[:,0:DataPoinstsNumber]
    neuralInputsNumber = DataPoinstsNumber

def restoreDataSize():
    global x, test_x, neuralInputsNumber
    x, y, test_x, test_y = modelUtils.getTrainAndTestData()
    neuralInputsNumber = configs.neuralInputsNumber

def createModels_for_classification():
    suffix ="classification"
    dnn_model(get_ddn_layers(),suffix)
    rf_model(suffix)
    svm_model(suffix)

def createModels_for_classification_quadraticTrend():
    global  x, y, test_x, test_y
    from simulation import quadraticTrend
    x, y, test_x, test_y = modelUtils.getTrainAndTestData()
    print("number of Target Variable is", len(test_y[0]))
    suffix =configs.model_suffix_classification_quadraticTrend
    dnn_model(get_dnn_layers_for(layer_list_quad),suffix)
    rf_model(suffix)
    svm_model(suffix)

def createModels_for_prediction():
    suffix ="prediction"
    upateDataSize()
    dnn_model(get_ddn_layers_pred(),suffix)
    rf_model(suffix)
    svm_model(suffix)
    restoreDataSize()


def creatModelfor(suffix,list):
    global  x, y, test_x, test_y
    x, y, test_x, test_y = modelUtils.getTrainAndTestData()
    print("number of Target Variable is", len(test_y[0]))
    dnn_model(get_dnn_layers_for(list),suffix)
    rf_model(suffix)
    svm_model(suffix)

def createModels_for_7x_m13_randShift():
    suffix = simulationProfile.setProfile_7x_m13_randShift()[:-1]
    creatModelfor(suffix,layer_list_for_7x_m13)

def createModels_for_7x_m08_randShift():
    suffix = simulationProfile.setProfile_7x_m08_randShift()[:-1]
    creatModelfor(suffix,layer_list_for_7x_m08)

def createModels_for_7x_m04_randShift():
    suffix = simulationProfile.setProfile_7x_m04_randShift()[:-1]
    creatModelfor(suffix,layer_list_for_7x_m04)

def creat_all():
    createModels_for_classification()
    createModels_for_prediction()