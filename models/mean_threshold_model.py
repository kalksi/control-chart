import os
import re
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("Configurations"))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
import Configurations
from utils import dataframeUtil
from utils import statisticsUtils
from utils import chartChecker as check
from utils import modelUtils
from utils import plotUtils
import numpy as np
import matplotlib.pyplot as plt

dir = "../"
min_val, max_val = 0,0
epoch = 41

def getTrainingsdata():
    x_shiftUp,y_shiftUp=dataframeUtil.getDataset(dir+Configurations.shiftUptrainDatasetName,Configurations.neuralInputsNumber,1)
    x_shiftDown,y_shiftDown = dataframeUtil.getDataset(dir+Configurations.shiftDowntrainDatasetName,Configurations.neuralInputsNumber,1)
    checkMuster = all(y_shiftUp>0) and all(y_shiftDown>0)
    if(not checkMuster):
        error_msg ="Error, alle Trainingdaten des  Shift-Musters muss mit y = 0 enthalten, pls change Configurations.generatedTrainDataOutputNumber_False to 0 "
        print(error_msg)
    return x_shiftUp,x_shiftDown

def getShiftMeanThreshold(x_number):        
    x_shiftUp,x_shiftDown = getTrainingsdata()
    means_shiftUp, _ = statisticsUtils.getNormalDistParam(x_shiftUp[:,0:x_number])
    means_shiftDown, _ = statisticsUtils.getNormalDistParam(x_shiftDown[:,0:x_number])
    min_val, max_val = np.min(means_shiftDown), np.max(means_shiftUp)
    return min_val, max_val

def getModelAccurancy():
    modelUtils.mainDir=dir
    x, y, test_x, test_y = modelUtils.getTrainAndTestData()
    predictions = predict(test_x)
    accurancy,confusionMatrix,report = modelUtils.getMetric(predictions,test_y)
    return accurancy

def findeOptimalSepration_maxval():
    global max_val
    lernrate = 0.01
    direction=1
    history=[]
    old_result = 0
    new_result = getModelAccurancy()
    for round in range(epoch):
        if( new_result < old_result):direction=direction*-1
        max_val = max_val + (direction*lernrate)
        old_result = new_result
        new_result = getModelAccurancy()
        history.append(new_result)
    plotUtils.plotCurve(history)

def findeOptimalSepration_minval():
    global min_val
    lernrate = 0.01
    direction=1
    history=[]
    old_result = 0
    new_result = getModelAccurancy()
    for round in range(epoch):
        if( new_result < old_result):direction=direction*-1
        min_val = min_val + (direction*lernrate)
        old_result = new_result
        new_result = getModelAccurancy()
        history.append(new_result)
    plotUtils.plotCurve(history)

def trainModel(x_number):
    global min_val, max_val
    min_val, max_val = getShiftMeanThreshold(x_number)
    print(min_val, max_val)
    findeOptimalSepration_maxval()
    findeOptimalSepration_minval()
    print(min_val, max_val)
    print("done ...")

def isTrendup(x, max_val):
    mean = np.mean(x)
    return True if (mean > max_val) else False

def isTrendDown(x, min_val):
    mean = np.mean(x)
    return True if (mean < min_val) else False

def predictOne(chart):
    predictions = []
    result = isTrendup(chart, max_val)
    if (result):
        predictions.append([1,0,0,0,0])
        return predictions
    result = isTrendDown(chart, min_val)
    if (result):
        predictions.append([0,1,0,0,0])
        return predictions
    result = check.isShiftUp(chart)
    if (result):
        predictions.append([0,0,1,0,0])
        return predictions
    result = check.isShiftDown(chart)
    if (result):
        predictions.append([0,0,0,1,0])
        return predictions
    predictions.append([0,0,0,0,1])
    return predictions

def predict(x):
    predictions = []
    for chart in x:
      predictions.append(predictOne(chart)[0])
      #print("predict for:",chart)
      #print("result:",predictOne(chart)[0])
    return predictions
