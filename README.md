# Control chart



## Setup

- Python 3.x installieren
- Danach in der Konsole folgende Bibliotheken installieren:

```
pip install pandas numpy matplotlib seaborn xlwt
pip install sklearn
pip install tensorflow
pip install pydot xlrd
```
***
- Zusätzlich 

You must install pydot (`pip install pydot`) and install graphviz (see instructions at https://graphviz.gitlab.io/download/) for plot_model/model_to_dot to work.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/kalksi/control-chart.git
git branch -M main
git push -uf origin main
```



***

# Editing this README

