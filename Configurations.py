import math

# Chart configuration
mean = 10
standardDeviation = 4
n = 7  # Number of Subgroups
sigma = standardDeviation/0.97
M_0 = 0  # Prozess Median
w = 1.96
k = 3

W_u = M_0 - ((w*sigma)/math.sqrt(n))
W_o = M_0 + ((w*sigma)/math.sqrt(n))

K_u = M_0 - ((k*sigma)/math.sqrt(n))
K_o = M_0 + ((k*sigma)/math.sqrt(n))

# Optimize Simulation with Linear Regression
natural_optimize_simulation_with_given_slope = True
trend_optimize_simulation_with_given_slope = True
slope_no_trend_min = -0.3
slope_no_trend_max = 0.3
slope_trend_up_min = 0.2
slope_trend_Down_max = -0.2
trend_meanslist_slope = 1.3

# Simulation and trainingsdata configs
neuralInputsNumber = n
numberOftargetVariables = 4
numberOfPointsInARow_shift = 5
numberOfPointsInARow_trend = 7
generatedTrainDataOutputNumber_True = 1000
generatedTrainDataOutputNumber_False = 0
trainingDataSum = generatedTrainDataOutputNumber_True + \
    generatedTrainDataOutputNumber_False
generateTestData = True
labels = ['Trend-Up', 'Trend-Down', 'Shift-Up', 'Shift-Down', 'Natural']

# export configs
dataExportDir = "dataset/"
trainDatasetName = dataExportDir+"traindata.csv"
testDatasetName = dataExportDir+"testdata.csv"
trainDatasetNameQuadratic = dataExportDir+"Quadratic/traindata.csv"
testDatasetNameQuadratic = dataExportDir+"Quadratic/testdata.csv"

# natural (charts Without muster)
simulateNaturalcharts = True if (
    generatedTrainDataOutputNumber_False == 0) else False
natural_traindata_number = generatedTrainDataOutputNumber_True  # /4
targetColumName_natural = "natural"
natural_trainDatasetName = dataExportDir+"natural_traindata.csv"
natural_testDatasetName = dataExportDir+"natural_testdata.csv"
numberOftargetVariables = numberOftargetVariables + \
    1 if (simulateNaturalcharts) else numberOftargetVariables

# shift setting
randomShift = False
randomShift_start = 0.5
randomShift_end = 3.0

# shift up settings
shiftUptrainDatasetName = dataExportDir+"shiftup_traindata.csv"
shiftUptestDatasetName = dataExportDir+"shiftup_testdata.csv"
targetColumName_shiftUp = "shiftUp"
shiftUpChartPlotTitle = "Shift up Chart"
shift_tolerance = 0.1

# shift down settings
shiftDowntrainDatasetName = dataExportDir+"shiftdown_traindata.csv"
shiftDowntestDatasetName = dataExportDir+"shiftdown_testdata.csv"
targetColumName_shiftDown = "shiftDown"
shiftDownChartPlotTitle = "Shift down Chart"

# Trend up settings
trendUptrainDatasetName = dataExportDir+"trendup_traindata.csv"
trendUptestDatasetName = dataExportDir+"trendup_testdata.csv"
targetColumName_trendUp = "trendUp"
trendUpChartPlotTitle = "Trend up Chart"

# Trend down settings
trendDowntrainDatasetName = dataExportDir+"trenddown_traindata.csv"
trendDowntestDatasetName = dataExportDir+"trenddown_testdata.csv"
targetColumName_trendDown = "trendDown"
trendDownChartPlotTitle = "Trend down Chart"

# slowChanges up settings
slowChanges_faktor = 4
slowChanges_slope = 0.01  # 0.01
slowChangesUpChartPlotTitle = "Slow Change Up"
slowChangesUptrainDatasetName = dataExportDir+"slowChangesup_traindata.csv"
slowChangesUptestDatasetName = dataExportDir+"slowChangesup_testdata.csv"
targetColumName_slowChangesUp = "slowChangesUp"

# slowChanges down settings
slowChangesDowntrainDatasetName = dataExportDir+"slowChangesdown_traindata.csv"
slowChangesDowntestDatasetName = dataExportDir+"slowChangesdown_testdata.csv"
targetColumName_slowChangesDown = "slowChangesDown"
slowChangesDownChartPlotTitle = "slowChanges down Chart"

# quadraticTrend up settings
quadraticTrendMode = False
quadraticTrend_faktor = 4
quadraticTrend_slope = 0.01  # 0.01
quadraticTrendUpChartPlotTitle = "Quadratic Trend Up"
quadraticTrendUptrainDatasetName = dataExportDir+"quadraticTrendup_traindata.csv"
quadraticTrendUptestDatasetName = dataExportDir+"quadraticTrendup_testdata.csv"
targetColumName_quadraticTrendUp = "quadraticTrendUp"
model_suffix_classification_quadraticTrend = "classification_quadraticTrend"

# quadraticTrend down settings
quadraticTrendDowntrainDatasetName = dataExportDir + \
    "quadraticTrenddown_traindata.csv"
quadraticTrendDowntestDatasetName = dataExportDir+"quadraticTrenddown_testdata.csv"
targetColumName_quadraticTrendDown = "quadraticTrendDown"
quadraticTrendDownChartPlotTitle = "quadraticTrend down Chart"

# cnn
cnn_natural_trainDatasetName = "../dataset/cnn_natural_traindata.csv"
cnn_natural_testDatasetName = "../dataset/cnn_natural_testdata.csv"
cnn_n = n*slowChanges_faktor

# Plot settings
figSize = 5
plotPath = "plots/"

# model outputpath
model_output_path = "saved_model/"

# methods


def printConfigurations():
    print("Configuration report:")
    print("Number of Subgroups is: "+str(n))
    print("The Trend Simulation Formel is:", "x*" +
          str(trend_meanslist_slope)+"+"+str(mean))
    print("randomshift ist:",randomShift)
    print("shift-value=",randomShift_start," and ",randomShift_end)


printConfigurations()


def updateDataExportPath():
    global quadraticTrendUptrainDatasetName, quadraticTrendUptestDatasetName, quadraticTrendDowntrainDatasetName, quadraticTrendDowntestDatasetName, slowChangesDowntrainDatasetName, slowChangesDowntestDatasetName, slowChangesUptrainDatasetName, slowChangesUptestDatasetName, trendDowntrainDatasetName, trendDowntestDatasetName, trendUptrainDatasetName, trendUptrainDatasetName, trendUptestDatasetName, shiftDowntrainDatasetName, shiftDowntestDatasetName, shiftDowntestDatasetName, shiftUptrainDatasetName, shiftUptrainDatasetName, shiftUptestDatasetName, natural_trainDatasetName, natural_testDatasetName, trainDatasetName, testDatasetName, trainDatasetNameQuadratic, testDatasetNameQuadratic

    quadraticTrendUptrainDatasetName = dataExportDir+"quadraticTrendup_traindata.csv"
    quadraticTrendUptestDatasetName = dataExportDir+"quadraticTrendup_testdata.csv"
    quadraticTrendDowntrainDatasetName = dataExportDir + \
        "quadraticTrenddown_traindata.csv"
    quadraticTrendDowntestDatasetName = dataExportDir+"quadraticTrenddown_testdata.csv"

    slowChangesDowntrainDatasetName = dataExportDir+"slowChangesdown_traindata.csv"
    slowChangesDowntestDatasetName = dataExportDir+"slowChangesdown_testdata.csv"
    slowChangesUptrainDatasetName = dataExportDir+"slowChangesup_traindata.csv"
    slowChangesUptestDatasetName = dataExportDir+"slowChangesup_testdata.csv"

    trendDowntrainDatasetName = dataExportDir+"trenddown_traindata.csv"
    trendDowntestDatasetName = dataExportDir+"trenddown_testdata.csv"
    trendUptrainDatasetName = dataExportDir+"trendup_traindata.csv"
    trendUptestDatasetName = dataExportDir+"trendup_testdata.csv"

    shiftDowntrainDatasetName = dataExportDir+"shiftdown_traindata.csv"
    shiftDowntestDatasetName = dataExportDir+"shiftdown_testdata.csv"
    shiftUptrainDatasetName = dataExportDir+"shiftup_traindata.csv"
    shiftUptestDatasetName = dataExportDir+"shiftup_testdata.csv"

    natural_trainDatasetName = dataExportDir+"natural_traindata.csv"
    natural_testDatasetName = dataExportDir+"natural_testdata.csv"

    trainDatasetName = dataExportDir+"traindata.csv"
    testDatasetName = dataExportDir+"testdata.csv"

    trainDatasetNameQuadratic = dataExportDir+"Quadratic/traindata.csv"
    testDatasetNameQuadratic = dataExportDir+"Quadratic/testdata.csv"
