# Import required libraries
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("Configurations"))))
from utils import chartChecker as check
from utils import simulaltionUtils
from utils import dataframeUtil
import Configurations as configs
from random import randrange
from random import uniform

#numberOfPointsInARow_shift = configs.numberOfPointsInARow_shift

def generateShiftMeanList(shiftStartPoint,numberOfSubgroups ,shiftValue):
    meansList = []
    #shiftEndPoint = randrange(shiftStartPoint+configs.numberOfPointsInARow_shift,numberOfSubgroups+1)
    shiftEndPoint = numberOfSubgroups-1
    m_0 = configs.mean
    for mean in range(numberOfSubgroups):
        if(mean >= shiftStartPoint and mean <= shiftEndPoint):
            meansList.append(m_0+shiftValue)   
        else:
            meansList.append(m_0)
    return meansList

def getTrainDatasetName(shiftFaktor):
    return configs.shiftDowntrainDatasetName if(shiftFaktor < 0) else configs.shiftUptrainDatasetName

def getTestDatasetName(shiftFaktor):
    return configs.shiftDowntestDatasetName if(shiftFaktor < 0) else configs.shiftUptestDatasetName

def getTargetCloumName(shiftFaktor):
    targetColumName =""
    if(shiftFaktor < 0):
        targetColumName = configs.targetColumName_shiftDown
    else:
        targetColumName = configs.targetColumName_shiftUp
    return targetColumName

def simulateOneShiftChart(meanList,shiftFaktor):
    stop = False
    chart=[]
    while(not stop):
        chart=simulaltionUtils.samplesGenerator(meanList,configs.standardDeviation)
        stop = check.checkShift(0,chart,configs.numberOfPointsInARow_shift,shiftFaktor) and not check.checkShift(0,chart,configs.numberOfPointsInARow_shift,-1*shiftFaktor) # and check.checkNotTrend(chart,configs.slope_no_trend_min,configs.slope_no_trend_max)
    return chart

def simulateShiftTrainData(numberTrue,numberFalse,mean,meanList,shiftFaktor):
    x = []
    y = []
    pointsDic = {
        "x": [[]],
        "y": []
    }
    #generate charts with a Shift  
    shiftStartPoint = 1
    for _ in range(numberTrue):
     if (configs.randomShift):
        direction = 1 if shiftFaktor >0 else -1
        shift= uniform(configs.randomShift_start , configs.randomShift_end) *direction
        #shiftStartPoint = randrange(0,configs.neuralInputsNumber-configs.numberOfPointsInARow_shift)
        shiftStartPoint=0
        meanList = generateShiftMeanList(shiftStartPoint,configs.neuralInputsNumber,shift)
     x.append(simulateOneShiftChart(meanList,shiftFaktor))
     y.append(1)
    #generate charts without a shift
    for _ in range(numberFalse):
      x.append(simulaltionUtils.simulateOneNaturalChart(mean,len(meanList)))
      y.append(0)
     
    pointsDic["x"] = x
    pointsDic["y"] = y
    return pointsDic

def setup(shiftFaktor):
    meanList = generateShiftMeanList(2,configs.neuralInputsNumber,shiftFaktor)
    trainData = simulateShiftTrainData(configs.generatedTrainDataOutputNumber_True,configs.generatedTrainDataOutputNumber_False,configs.mean,meanList,shiftFaktor)
    df = dataframeUtil.setUpDataFrame(configs.neuralInputsNumber, getTargetCloumName(shiftFaktor))
    #df = dataframeUtil.setUpDataFrame(configs.neuralInputsNumber, configs.targetColumName_shiftup)
    df = dataframeUtil.fillDataFrame(df, trainData)
    return df

def start(shiftFaktor):
    df = setup(shiftFaktor)
    if(configs.generateTestData):
        dataframeUtil.exportDataFrame(df, getTrainDatasetName(shiftFaktor))
        df = setup(shiftFaktor)
        dataframeUtil.exportDataFrame(df, getTestDatasetName(shiftFaktor))
    else:
        dataframeUtil.exportDataFrame(df, getTrainDatasetName(shiftFaktor))
    print(getTargetCloumName(shiftFaktor)+" Simulation ist fertig..")