import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("Configurations"))))
import Configurations
from utils import simulaltionUtils
from utils import dataframeUtil

# global Confgis
slowChanges_faktor = Configurations.slowChanges_faktor
slope = Configurations.slowChanges_slope
timeSeriesSize = Configurations.n * slowChanges_faktor
mean  = Configurations.mean
standardDeviation= Configurations.standardDeviation
neuralInputsNumber= timeSeriesSize
trendStartPoint = 1
trendEndPoint = timeSeriesSize
optimize_simulation_with_given_slope = False
numberOfPointsInARow_trend=Configurations.numberOfPointsInARow_trend

Configurations.generatedTrainDataOutputNumber_True = 10
Configurations.neuralInputsNumber = timeSeriesSize
Configurations.generateTestData= False

configDic = {
    "targetColumName":"",
    "slowChangesTrainDatasetName":"",
    "slowChangesTestDatasetName":"",
}

# Methods
def setConfigParam(direction):
 if direction == 1:
    configDic["targetColumName"] =Configurations.targetColumName_slowChangesUp
    configDic["slowChangesTrainDatasetName"] =Configurations.slowChangesUptrainDatasetName
    configDic["slowChangesTestDatasetName"] =Configurations.slowChangesUptestDatasetName
 else:
    configDic["targetColumName"] =Configurations.targetColumName_slowChangesDown
    configDic["slowChangesTrainDatasetName"] =Configurations.slowChangesDowntrainDatasetName
    configDic["slowChangesTestDatasetName"] =Configurations.slowChangesDowntestDatasetName

def setup(direction):
    setConfigParam(direction)
    trainData = simulaltionUtils.simulateTrend(direction,trendStartPoint,trendEndPoint,timeSeriesSize,slope,optimize_simulation_with_given_slope)
    df = dataframeUtil.setUpDataFrame(neuralInputsNumber,configDic["targetColumName"])
    df = dataframeUtil.fillDataFrame(df, trainData)
    return df

def start_simulation(direction):
    df = setup(direction)
    if(Configurations.generateTestData):
        dataframeUtil.exportDataFrame(df, configDic["slowChangesTrainDatasetName"])
        df = setup(direction)
        dataframeUtil.exportDataFrame(df, configDic["slowChangesTestDatasetName"])
    else:
        dataframeUtil.exportDataFrame(df, configDic["slowChangesTrainDatasetName"])

def start(direction):
    start_simulation(direction)
    print("Slow Change {0} Simulation is done ..".format("up" if direction==1 else "down"))
   