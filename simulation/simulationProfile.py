from distutils.command.config import config
import os
import sys
from tkinter import N
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("Configurations"))))
import Configurations as configs
from utils import simulaltionUtils
from utils import dataframeUtil

dataExportDir= configs.dataExportDir

def setProfile_7x_m13_randShift():
    exportSuffix="7x_m13_randShift/"
    configs.dataExportDir = dataExportDir+exportSuffix
    configs.n=7
    configs.neuralInputsNumber=configs.n
    #configs.trend_optimize_simulation_with_given_slope=False
    configs.randomShift=True
    print("n=",configs.n,"trend_optimize=",configs.trend_optimize_simulation_with_given_slope,"randomShift=",configs.randomShift)
    print ("exportSuffix=",exportSuffix)
    configs.updateDataExportPath()
    return exportSuffix

def setProfile_7x_m08_randShift():
    exportSuffix="7x_m08_randShift/"
    configs.dataExportDir = dataExportDir+exportSuffix
    configs.n=7
    configs.neuralInputsNumber=configs.n
    configs.trend_meanslist_slope=0.8
    #configs.trend_optimize_simulation_with_given_slope=False
    configs.randomShift=True
    print("n=",configs.n,"trend_optimize=",configs.trend_optimize_simulation_with_given_slope,"randomShift=",configs.randomShift)
    print ("exportSuffix=",exportSuffix)
    configs.updateDataExportPath()
    return exportSuffix

def setProfile_7x_m04_randShift():
    exportSuffix="7x_m04_randShift/"
    configs.dataExportDir = dataExportDir+exportSuffix
    configs.n=7
    configs.neuralInputsNumber=configs.n
    configs.trend_meanslist_slope=0.4
    #configs.trend_optimize_simulation_with_given_slope=False
    configs.randomShift=True
    print("n=",configs.n,"trend_optimize=",configs.trend_optimize_simulation_with_given_slope,"randomShift=",configs.randomShift)
    print ("exportSuffix=",exportSuffix)
    configs.updateDataExportPath()
    return exportSuffix