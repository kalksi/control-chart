import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("Configurations"))))
import Configurations
from utils import simulaltionUtils
from utils import dataframeUtil
from random import uniform

#
print("quadraticTrend ist imported... Globale Setting will be changed")
# global Confgis
slowChanges_faktor = Configurations.slowChanges_faktor
slope = Configurations.slowChanges_slope
timeSeriesSize = Configurations.n * slowChanges_faktor
mean  = Configurations.mean
standardDeviation= Configurations.standardDeviation
neuralInputsNumber= timeSeriesSize
trendStartPoint = 1
trendEndPoint = timeSeriesSize
optimize_simulation_with_given_slope = False
numberOfPointsInARow_trend=Configurations.numberOfPointsInARow_trend

#change main congfis
#Configurations.generatedTrainDataOutputNumber_True = 1000
Configurations.neuralInputsNumber = timeSeriesSize
Configurations.n = timeSeriesSize
Configurations.quadraticTrendMode=True
Configurations.randomShift=True
Configurations.trend_meanslist_slope=0.12
Configurations.numberOftargetVariables= 7
Configurations.labels= ['Trend-Up','Trend-Down','Shift-Up','Shift-Down','QTrend-UP','QTrend-Down','Natural']

#Configurations.generateTestData= False
# Configurations.trendUptrainDatasetName = Configurations.quadraticTrendUptrainDatasetName
# Configurations.trendUpChartPlotTitle = Configurations.quadraticTrendUpChartPlotTitle
# Configurations.trendDowntrainDatasetName = Configurations.quadraticTrendDowntrainDatasetName
# Configurations.trendDownChartPlotTitle = Configurations.quadraticTrendDownChartPlotTitle

configDic = {
    "targetColumName":"",
    "quadraticTrendTrainDatasetName":"",
    "quadraticTrendTestDatasetName":"",
}

# Methods
def setConfigParam(direction):
 if direction == 1:
    configDic["targetColumName"] =Configurations.targetColumName_quadraticTrendUp
    configDic["quadraticTrendTrainDatasetName"] =Configurations.quadraticTrendUptrainDatasetName
    configDic["quadraticTrendTestDatasetName"] =Configurations.quadraticTrendUptestDatasetName
 else:
    configDic["targetColumName"] =Configurations.targetColumName_quadraticTrendDown
    configDic["quadraticTrendTrainDatasetName"] =Configurations.quadraticTrendDowntrainDatasetName
    configDic["quadraticTrendTestDatasetName"] =Configurations.quadraticTrendDowntestDatasetName

def getTrendQuadraticMeanList(direction,trendStartPoint,trendEndPoint,timeSeriesSize,a,b):
    meansList = []
    x = 1  
    for mean_index in range(timeSeriesSize):
        if(mean_index >= trendStartPoint and mean_index < trendEndPoint):
            meansList.append(direction*(a*(x*x)+b*x) + mean )
            x = x + 1
        else:
            meansList.append(mean)
    return meansList

def simulateQuadraticTrend(direction,trendStartPoint,trendEndPoint,timeSeriesSize):
    foundTrue = 0
    x = []
    y = []
    pointsDic = {
        "x": [[]],
        "y": []
    }
    while (foundTrue < Configurations.generatedTrainDataOutputNumber_True):
        faktor=uniform(1.0 , 1.5)
        a = uniform(.016 , .025) *faktor *-1
        b = uniform(0.39 , .5)*faktor
        meansList = getTrendQuadraticMeanList(direction,trendStartPoint,trendEndPoint,timeSeriesSize,a,b)
        chart = simulaltionUtils.samplesGenerator(meansList,  Configurations.standardDeviation)
        x.append(chart)
        y.append(1)
        foundTrue = foundTrue+1
        pointsDic["x"] = x
        pointsDic["y"] = y    
    return pointsDic


def setup(direction):
    setConfigParam(direction)
    trainData = simulateQuadraticTrend(direction,trendStartPoint,trendEndPoint,timeSeriesSize)
    df = dataframeUtil.setUpDataFrame(neuralInputsNumber,configDic["targetColumName"])
    df = dataframeUtil.fillDataFrame(df, trainData)
    return df

def start_simulation(direction):
    df = setup(direction)
    if(Configurations.generateTestData):
        dataframeUtil.exportDataFrame(df, configDic["quadraticTrendTrainDatasetName"])
        df = setup(direction)
        dataframeUtil.exportDataFrame(df, configDic["quadraticTrendTestDatasetName"])
    else:
        dataframeUtil.exportDataFrame(df, configDic["quadraticTrendTrainDatasetName"])

def start(direction):
    start_simulation(direction)
    print("Quadratic Trend{0} Simulation is done ..".format("up" if direction==1 else "down"))
   