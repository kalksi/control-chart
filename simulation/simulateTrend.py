import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("Configurations"))))
import Configurations
from utils import simulaltionUtils
from utils import dataframeUtil

# global Confgis
#slope = Configurations.trend_meanslist_slope
timeSeriesSize = Configurations.n
mean  = Configurations.mean
standardDeviation= Configurations.standardDeviation
neuralInputsNumber= Configurations.neuralInputsNumber
trendStartPoint = 1
trendEndPoint = Configurations.n
numberOfPointsInARow_trend=Configurations.numberOfPointsInARow_trend
optimize_simulation_with_given_slope = Configurations.trend_optimize_simulation_with_given_slope


configDic = {
    "targetColumName":"",
    "trendTrainDatasetName":"",
    "trendTestDatasetName":"",
}

# Methods
def setConfigParam(direction):
 if direction == 1:
    configDic["targetColumName"] =Configurations.targetColumName_trendUp
    configDic["trendTrainDatasetName"] =Configurations.trendUptrainDatasetName
    configDic["trendTestDatasetName"] =Configurations.trendUptestDatasetName
 else:
    configDic["targetColumName"] =Configurations.targetColumName_trendDown
    configDic["trendTrainDatasetName"] =Configurations.trendDowntrainDatasetName
    configDic["trendTestDatasetName"] =Configurations.trendDowntestDatasetName


def setup(direction):
    setConfigParam(direction)
    trainData = simulaltionUtils.simulateLinearTrend(direction,trendStartPoint,trendEndPoint,timeSeriesSize,Configurations.trend_meanslist_slope,optimize_simulation_with_given_slope)
    df = dataframeUtil.setUpDataFrame(neuralInputsNumber,configDic["targetColumName"])
    df = dataframeUtil.fillDataFrame(df, trainData)
    return df

def start_simulation(direction):
    df = setup(direction)
    if(Configurations.generateTestData):
        dataframeUtil.exportDataFrame(df, configDic["trendTrainDatasetName"])
        df = setup(direction)
        dataframeUtil.exportDataFrame(df, configDic["trendTestDatasetName"])
    else:
        dataframeUtil.exportDataFrame(df, configDic["trendTrainDatasetName"])

def start(direction):
    start_simulation(direction)
    print("Trend {0} Simulation is done ..".format("up" if direction==1 else "down"))
   