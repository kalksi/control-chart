import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("Configurations"))))
#sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
from utils import modelUtils
import Configurations as configs
import numpy as np
import pandas as pd
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.metrics import precision_recall_fscore_support as score
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.metrics import precision_recall_fscore_support as score

suffix=""
def getMetric(y_predictions,test_y):
    y_predictions_flatten=np.argmax(y_predictions, axis=1)
    test_y_flatten=np.argmax(test_y, axis=1)
    confusionMatrix = confusion_matrix(test_y_flatten,y_predictions_flatten)
    report = classification_report(test_y_flatten,y_predictions_flatten)
    accurancy = accuracy_score(test_y_flatten, y_predictions_flatten)
    return accurancy,confusionMatrix,report

def getConfusionMatix(y_pred, y_test, path,modelName):
    accurancy, confusionMatrix, report = modelUtils.getMetric(y_pred, y_test)
    cm = np.asanyarray(confusionMatrix)
    df = pd.DataFrame(cm, columns=configs.labels)
    df.index = configs.labels
    df.to_excel(path+suffix+"confusionMatrix_"+modelName+".xls")
    return df

def getConfusionMatix_multy_classes(y_pred, y_test,path, modelName):
    FP, FN, TP, TN = modelUtils.get_fb_fn_tp_tn(y_pred, y_test)
    dic = {
        "FP": FP, "FN": FN, "TP": TP, "TN": TN
    }
    df = pd.DataFrame(dic)
    df.index = configs.labels
    precision, recall, fscore, accuracy = modelUtils.calculateScores(
        y_pred, y_test)
    df["Precision"] = precision
    df["Recall"] = recall
    df["FScore"] = fscore
    df["Accuracy"] = accuracy
    df.loc['Average'] = df.mean()
    df.to_excel(path+suffix+"confusionMatrix_Multy_"+modelName+".xls")
    return df
