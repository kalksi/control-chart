import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("Configurations"))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
from utils import dataframeUtil
from utils import plotUtils
from tensorflow import keras
import Configurations as configs
import numpy as np
import pandas as pd
from random import randrange
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.metrics import precision_recall_fscore_support as score
import statistics


#neuralInputsNumber = configs.neuralInputsNumber
#targetVarNumber = configs.numberOftargetVariables
mainDir = "../"
print("main dir:",mainDir)
# transform
def transform_y_to_one_dimension(y):
    #y_transformed = y * [1,2,3,4,5] 
    y_transformed = y * list(range(1, configs.numberOftargetVariables+1))
    y_d1 = np.max(y_transformed, axis=1)
    return y_d1

def transform_y_to_binary(y):
    transformed_y = []
    for decimal_y in y:
        binary_y = [0] * configs.numberOftargetVariables
        binary_y[int(decimal_y)-1] = 1
        transformed_y.append(binary_y)
    return np.asanyarray(transformed_y)

#load trainingsdata utils
def getShiftUpTrainAndTestData():
    pathTraindata=mainDir+configs.shiftUptrainDatasetName
    pathTestdata=mainDir+configs.shiftUptestDatasetName
    neuralInputsNumber = configs.neuralInputsNumber
    return dataframeUtil.getTrainAndTestData(pathTraindata,pathTestdata,configs.numberOftargetVariables,neuralInputsNumber)

# def getTrainAndTestData(mainDir,targetVarNumber):
#     pathTraindata=mainDir+configs.trainDatasetName
#     pathTestdata=mainDir+configs.testDatasetName
#     return dataframeUtil.getTrainAndTestData(pathTraindata,pathTestdata,targetVarNumber,neuralInputsNumber)

def getTrainAndTestData():
    targetVarNumber= configs.numberOftargetVariables
    pathTraindata=mainDir+ (configs.trainDatasetName if(not configs.quadraticTrendMode) else configs.trainDatasetNameQuadratic)
    #print("getting data form:",pathTraindata)
    pathTestdata=mainDir+( configs.testDatasetName if(not configs.quadraticTrendMode) else configs.testDatasetNameQuadratic)
    return dataframeUtil.getTrainAndTestData(pathTraindata,pathTestdata,targetVarNumber,configs.neuralInputsNumber)

def getShiftDownTrainAndTestData():
    pathTraindata=mainDir+configs.shiftDowntrainDatasetName
    pathTestdata=mainDir+configs.shiftDowntestDatasetName
    return dataframeUtil.getTrainAndTestData(pathTraindata,pathTestdata,configs.numberOftargetVariables,configs.neuralInputsNumber)

# models
def getModel(x, y, test_x, test_y):
    model = keras.Sequential([
        keras.layers.Dense(256, input_shape=(configs.neuralInputsNumber,), activation='relu'),
        keras.layers.Dense(256, activation='relu'),
        keras.layers.Dropout(0.3),
        keras.layers.Dense(512, activation='relu'),
        keras.layers.Dropout(0.3),
        keras.layers.Dense(128, activation='relu'),
        keras.layers.Dropout(0.4),
        keras.layers.Dense(512, activation='relu'),
        #keras.layers.Dropout(0.4),
        keras.layers.Dense(configs.numberOftargetVariables, activation='sigmoid')])

    model.compile(optimizer='adam', 
                loss=keras.losses.BinaryCrossentropy(from_logits=True),
                metrics=['accuracy'])

    model.fit(x, y, batch_size=32, epochs=60)
    print("EVALUATION",model.evaluate(test_x, test_y))
    return model

def saveShiftDownModel():
    x,y,test_x,test_y = getShiftDownTrainAndTestData()
    model = getModel(x, y, test_x, test_y)
    model.save('model/shiftDownModel')
    return model

def saveShiftUpModel():
    x,y,test_x,test_y = getShiftUpTrainAndTestData()
    model = getModel(x, y, test_x, test_y)
    model.save('model/shiftUpModel')
    return model
# metrics
def getMetric(y_predictions,test_y):
    y_predictions_flatten=np.argmax(y_predictions, axis=1)
    test_y_flatten=np.argmax(test_y, axis=1)
    confusionMatrix = confusion_matrix(test_y_flatten,y_predictions_flatten)
    report = classification_report(test_y_flatten,y_predictions_flatten)
    accurancy = accuracy_score(test_y_flatten, y_predictions_flatten)
    return accurancy,confusionMatrix,report

def getScores(y_predictions,test_y):
    y_predictions_flatten=np.argmax(y_predictions, axis=1)
    test_y_flatten=np.argmax(test_y, axis=1)
    precision, recall, fscore, support = score(test_y_flatten, y_predictions_flatten)
    accurancy = accuracy_score(test_y_flatten, y_predictions_flatten)
    return precision, recall, fscore, support, accurancy

def getMacroScore(y_predictions,test_y):
    y_predictions_flatten=np.argmax(y_predictions, axis=1)
    test_y_flatten=np.argmax(test_y, axis=1)
    p_macro, r_macro, f_macro, support_macro = score(y_true=test_y_flatten, y_pred=y_predictions_flatten, average='macro')
    accurancy = accuracy_score(test_y_flatten, y_predictions_flatten)
    return  p_macro, r_macro, f_macro , accurancy

# manuel scores
def get_fb_fn_tp_tn(y_pred,y_test):
    accurancy,confusionMatrix,report = getMetric(y_pred,y_test)
    FP = confusionMatrix.sum(axis=0) - np.diag(confusionMatrix)  
    FN = confusionMatrix.sum(axis=1) - np.diag(confusionMatrix)
    TP = np.diag(confusionMatrix)
    TN = confusionMatrix.sum() - (FP + FN + TP)
    return FP,FN,TP,TN

def calculateScores(y_pred,y_test):
    FP,FN,TP,TN=get_fb_fn_tp_tn(y_pred,y_test)
    # Sensitivity, hit rate, recall, or true positive rate
    recall = TP/(TP+FN)
    # Precision or positive predictive value
    precision = TP/(TP+FP)
    # fscore
    fscore = 2*precision*recall/(precision+recall)
    # Overall accuracy
    accuracy = (TP+TN)/(TP+FP+FN+TN)
    return precision,recall,fscore,accuracy

def getMeanScores(y_pred,y_test):
    precision,recall,fscore,accuracy = calculateScores(y_pred,y_test)
    return statistics.mean(precision),statistics.mean(recall),statistics.mean(fscore),statistics.mean(accuracy)

def roundPredictionsResults(prediction):
  results = np.zeros(prediction.shape)
  results[0][np.argmax(prediction, axis=1) ] = 1
  return results

def roundPred(prediction):
  y_list = np.zeros(prediction.shape)
  for i in range (len(prediction)):
    y_list[i][np.argmax(prediction[i]) ] = 1
  return y_list

def getFalseClassifications(model, test_x, test_y):
    wrongClassifications = []
    shouldClassifications = []
    for index in range(len(test_x)):
        prediction = model.predict(test_x[index:index+1])
        prediction = roundPredictionsResults(prediction)
        isPredectionTrue = all((test_y[index:index+1] == prediction)[0])
        if(not isPredectionTrue):
           wrongClassifications.append(test_x[index])
           shouldClassifications.append(test_y[index])
    print("False Predictions Number: ",len(wrongClassifications)," from: ",len(test_x))
    # this method return wrongClassificationsCharts,shouldClassifications
    return np.asarray(wrongClassifications, dtype=np.float32),np.asarray(shouldClassifications, dtype=np.int32)

def getFalseClassificationsReport(model,wrongClassificatedsCharts,shouldClassifications):
    wrong_classification_list = []
    should_classification_list = []
    report = {
        "Wrong Classification": [],
        "Should Classification": []
    }
    for e in range(len(wrongClassificatedsCharts)):
        chart = wrongClassificatedsCharts[e:e+1]
        classification = model.predict(chart)
        classification = roundPredictionsResults(classification)
        should_classification = shouldClassifications[e:e+1]
        wrong_classification_label = getDetectionLabel_for_5y(classification)
        should_classification_label = getDetectionLabel_for_5y(should_classification)
        wrong_classification_list.append(wrong_classification_label[0])
        should_classification_list.append(should_classification_label[0])
    report["Wrong Classification"] = wrong_classification_list
    report["Should Classification"] = should_classification_list
    print(len(wrong_classification_list),len(should_classification_list))
    table = pd.DataFrame(report)
    table_with_count_of_duplicated = table.groupby(table.columns.tolist(),as_index=False).size()
    sum = table_with_count_of_duplicated["size"].sum() 
    table_with_count_of_duplicated['%'] = round(100 * table_with_count_of_duplicated["size"]/sum,2)
    grouped_table = table_with_count_of_duplicated.groupby(['Wrong Classification',"Should Classification"]).sum()
    summarized_tabel = table_with_count_of_duplicated.groupby(['Wrong Classification']).sum()
    return table,table_with_count_of_duplicated,grouped_table,summarized_tabel

def getFalseClassifications_svm(model, test_x, test_y):
    wrongClassifications = []
    shouldClassifications = []
    for index in range(len(test_x)):
        prediction = model.predict(test_x[index:index+1])
        binary = [0,0,0,0,0]
        binary[int(prediction)-1]=1
        prediction = np.asanyarray([binary])
        isPredectionTrue = all((test_y[index:index+1] == prediction)[0])
        if(not isPredectionTrue):
           wrongClassifications.append(test_x[index])
           shouldClassifications.append(test_y[index])
    print("False Predictions Number: ",len(wrongClassifications)," from: ",len(test_x))
    # this method return wrongClassificationsCharts,shouldClassifications
    return np.asarray(wrongClassifications, dtype=np.float32),np.asarray(shouldClassifications, dtype=np.int32)

def getFalseClassificationsReport_svm(model,wrongClassificatedsCharts,shouldClassifications):
    wrong_classification_list = []
    should_classification_list = []
    report = {
        "Wrong Classification": [],
        "Should Classification": []
    }
    for e in range(len(wrongClassificatedsCharts)):
        chart = wrongClassificatedsCharts[e:e+1]
        classification = model.predict(chart)
        binary = [0,0,0,0,0]
        binary[int(classification)-1]=1
        classification = np.asanyarray( [binary])
        should_classification = shouldClassifications[e:e+1]
        wrong_classification_label = getDetectionLabel_for_5y(classification)
        should_classification_label = getDetectionLabel_for_5y(should_classification)
        wrong_classification_list.append(wrong_classification_label[0])
        should_classification_list.append(should_classification_label[0])
    report["Wrong Classification"] = wrong_classification_list
    report["Should Classification"] = should_classification_list
    print(len(wrong_classification_list),len(should_classification_list))
    table = pd.DataFrame(report)
    table_with_count_of_duplicated = table.groupby(table.columns.tolist(),as_index=False).size()
    sum = table_with_count_of_duplicated["size"].sum() 
    table_with_count_of_duplicated['%'] = round(100 * table_with_count_of_duplicated["size"]/sum,2)
    grouped_table = table_with_count_of_duplicated.groupby(['Wrong Classification',"Should Classification"]).sum()
    summarized_tabel = table_with_count_of_duplicated.groupby(['Wrong Classification']).sum()
    return table,table_with_count_of_duplicated,grouped_table,summarized_tabel

def getDetectionLabel_for_4y(prediction):
    tragetColums=configs.labels
    prediction = roundPredictionsResults(prediction)
    #prediction = np.round(prediction) 
    detection = []
    if(prediction[0][0] == 1):
        detection.append(tragetColums[0]) 
    if(prediction[0][1] == 1):
        detection.append(tragetColums[1])  
    if(prediction[0][2] == 1):
        detection.append( tragetColums[2])  
    if(prediction[0][3] == 1):
        detection.append(tragetColums[3])  
    return detection

def getDetectionLabel_for_5y(prediction):
    prediction = roundPredictionsResults(prediction)
    #prediction = np.round(prediction) 
    detection = getDetectionLabel_for_4y(prediction)
    if(prediction[0][4] == 1):
        detection.append(configs.labels[4])  
    return detection

def getRandomChart(x,y,plot):
    a = randrange(0,len(x))
    b = a+1
    chart = x[a:b]
    target = y[a:b]
    label=getDetectionLabel_for_5y(target)
    if plot:
        plotUtils.plotChart(chart[0],label)
    return chart,target,label