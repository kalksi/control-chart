import scipy.stats as stats
import pylab as pl
import numpy as np
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("Configurations"))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
from utils import statisticsUtils
from utils import dataframeUtil
from utils import modelUtils
import Configurations
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.ticker as plticker
from sklearn.linear_model import LinearRegression
import seaborn as sns

figsize= Configurations.figSize

def plotCurve(y):
    x = list(range(0, len(y)))
    plt.plot(x, y)
    plt.title("Optimierungsverlauf")
    plt.xlabel("Schritte")
    plt.ylabel("Genauigkeit")
    plt.savefig('plots/{}'.format("Optimierung-Verlauf"), dpi=300)
    plt.show()

def plotNormalDistribution(normalDistribution):
    h = np.sort(normalDistribution) 
    fit = stats.norm.pdf(h, np.mean(h), np.std(h))
    q25, q75 = np.percentile(h, [25, 75])
    bin_width = 2 * (q75 - q25) * len(h) ** (-1/3)
    if bin_width > 0.0:
        bins = round((h.max() - h.min()) / bin_width)
    else: bins = 7
    pl.plot(h,fit,'-o')
    pl.hist(h,density=True, stacked=True,color="g",alpha=0.5, histtype='bar', ec='black',bins=bins)
    plt.savefig('plots/{}'.format("plots"), dpi=300)
    pl.show()    

 
def getChartPlot_axs():
    fig, axs = plt.subplots(1, figsize=(figsize, 5), sharex=True)
    mean = Configurations.M_0
    axs.axhline(mean, color='green')
    axs.axhline(Configurations.W_o, color='blue', linestyle='dashed')
    axs.axhline(Configurations.W_u, color='blue', linestyle='dashed')
    axs.axhline(Configurations.K_o, color='red', linestyle='dashed')
    axs.axhline(Configurations.K_u, color='red', linestyle='dashed')
    axs.set(xlabel='Zeit', ylabel='Werte')
    return fig,axs


def plotChart(points, title):
    xSeries = pd.Series(points)
    fig, axs = getChartPlot_axs()
    axs.plot(xSeries, linestyle='-', marker='o', color='black')
    axs.set_title(title)
    plt.savefig('plots/{}_{}'.format("plots",title), dpi=300)
    return axs

def plot2Chart(chart_1,chart_2,title):
    xSeries_1 = pd.Series(chart_1)
    xSeries_2 = pd.Series(chart_2)
    fig, axs = getChartPlot_axs()
    axs.plot(xSeries_1, linestyle='-', marker='o', color='black',label="x")
    axs.plot(xSeries_2, linestyle='-', marker='o', color='olive',label="Recontructed x")
    plt.legend(loc="upper left")
    axs.set_title(title)
    plt.savefig('plots/{}_{}'.format("plots_2_charts",title), dpi=300)
    return axs

def plotCusumChart(Datapoints,S_o_history,S_u_history,k,H, title):
    fig, axs = plt.subplots(1, figsize=(figsize, 5), sharex=True)
    mean = Configurations.M_0
    axs.axhline(mean, color='green')
    axs.axhline(mean+k, color='blue', linestyle='dashed')
    axs.axhline(mean-k, color='blue', linestyle='dashed')
    axs.axhline(mean+ H, color='red', linestyle='dashed')
    axs.axhline(mean-H, color='red', linestyle='dashed')
    axs.set(xlabel='Zeit', ylabel='Werte')
    axs.plot(S_o_history, linestyle=':', marker='x', color='olive',label="S_o")
    axs.plot(S_u_history, linestyle=':', marker='x', color='m',label="S_u")
    axs.plot(Datapoints, linestyle='-', marker='o', color='black',label="x")
    plt.legend(loc="upper left")
    #plt.ylim(-1.5, 2.0)
    axs.set_title(title)
    plt.savefig('plots/Cusum_{}_{}'.format("plots",title), dpi=300)
    return axs
    
def plotChartWithoutTime(points, title):
    xSeries = pd.Series(points)
    fig, axs = getChartPlot_axs()
    axs.scatter([0] * len(xSeries),xSeries)
    axs.set_title(title)
    plt.savefig('plots/{}'.format("plots_without_time_"+title), dpi=300)
    return axs

def plotChartWithSlope(points, title,plotWithSlope):
    xSeries = pd.Series(points)
    fig, axs = getChartPlot_axs()
    axs.set_title(title)
    slope, inercept =plotSlope(points) if plotWithSlope else (0,0)
    plt.savefig('plots/{}'.format("Chart_with_Slope"), dpi=300)
    plt.show()
    plt.close()
    return slope, inercept


def showPlotsForData(trainData, title):
    msg = ""
    for i in range(len(trainData['y'])):
        if (trainData['y'][i] == 1):
            msg = title
        else:
            msg = "Not a " + title
        plotChart(trainData['x'][i], msg)
        plt.close()

def plotChartWith2Detection(chart,prediction,title1,title2):
    detection = ""
    if(prediction[0][0] == 1):
        detection = title1
    if(prediction[0][1] == 1):
        detection = title2
    plotChart(chart, detection)

def plotRandomChartFrom_csv_with_1y(path, chartTitle):
    x,y = dataframeUtil.getRandomChartFrom_csv(path)
    x = x.values
    y = y.values
    chartTitle = getDetectionLabelForOneTarget(y,chartTitle)
    plotChart(x[0], chartTitle)
    return x, y

def getDetectionLabelForOneTarget(y,chartTitle):
    if(y == 0):
        chartTitle  = "No " + chartTitle
    return chartTitle

def plotRandomChartFrom_csv_With_5y(path):
    x,y = dataframeUtil.getRandomChartFrom_csv(path)
    x = x.values
    y = y.values
    chartTitle = modelUtils.getDetectionLabel_for_5y(y)
    plotChart(x[0], chartTitle[0])
    return x, y

def plotSlope(y):
    x = np.arange(0, len(y), 1, float).reshape(-1, 1)
    lm = LinearRegression()
    lm.fit(x, y)
    y_pred = lm.predict(x)
    plt.scatter(x, y, color="black")
    plt.plot(x, y_pred, color="blue", linewidth=3)
    return  lm.coef_[0],  lm.intercept_

# def plotRegSlope(x,y):
#     x= np.array(x).reshape(-1, 1)
#     lm = LinearRegression()
#     lm.fit(x, y)
#     y_pred = lm.predict(x)
#     plt.scatter(x, y, color="black")
#     plt.plot(x, y_pred, color="blue", linewidth=3)
#     return  lm.coef_[0],  lm.intercept_

def plotMetricHistogram(x,title,subTitle):
    q25, q75 = np.percentile(x, [25, 75])
    bin_width = 2 * (q75 - q25) * len(x) ** (-1/3)
    if bin_width > 0.0:
        bins = round((x.max() - x.min()) / bin_width)
    else: bins = 7
    #print("Freedman–Diaconis number of bins:", bins)
    plt.hist(x,density=True,  bins= bins+1, alpha=0.5, histtype='bar', ec='black')
    mn, mx = plt.xlim()
    plt.xlim(mn, mx)
    kde_xs = np.linspace(mn, mx, 300)
    kde = stats.gaussian_kde(x)
    plt.plot(kde_xs, kde.pdf(kde_xs), label="PDF")
    #plt.legend(loc="upper left")
    plt.ylabel("Häufigkeitsdichte ")
    plt.xlabel(title+" (min: "+str(round(x.min(),3))+", max: "+ str(round(x.max(),3))+")")
    plt.title("Histogramm")
    plt.savefig(Configurations.plotPath+title+subTitle+"_Histogramm"'.png',dpi=300)
    plt.show()
    plt.close()
    sns.boxplot(x=x)
    plt.title("Whisker")
    plt.xlabel(title)
    plt.savefig(Configurations.plotPath+title+subTitle+'_Whisker.png',dpi=300)
    plt.show()
    plt.close()

def plotChartWithDetectionBox(chart,predctionStart,predctionTitle,classificationStart,classificationEnd,title):
    axs = plotChart(chart,title)
    axs.axvline(classificationStart, color='y',  linestyle='dashdot', marker='o')
    axs.axvline(classificationEnd, color='y',  linestyle='dashdot', marker='o')
    predctionStart = 0
    rect = patches.Rectangle((predctionStart, -5), 10, 10, linewidth=2, edgecolor='r', facecolor='none' ,  linestyle='dashdot')
    axs.add_patch(rect)
    axs.text((predctionStart+0.3), -4,predctionTitle, {'color': 'C0', 'fontsize': 13})
    plt.show()

def getHis_axs():
    fig, (ax1, ax2) = plt.subplots(nrows=2, sharex=True,figsize=(10, 12))
    #bins = np.arange(10,61,1)
    return fig, (ax1, ax2)

def add_kde(ax1,ax2,muster,label,color,bins):
    ax1.hist(muster, bins=bins, color=color, alpha=0.4, label=label)
    mn, mx = plt.xlim()
    x =np.linspace(mn, mx, 300)
    muster_kde = stats.gaussian_kde(muster)
    muster_curve = muster_kde(x)*muster.shape[0]
    ax2.plot(x, muster_curve, color=color)
    ax2.fill_between(x, 0, muster_curve, color=color, alpha=0.4, label=label)
    return ax1,ax2

def add_his(ax1,muster,label,color,bins):
    ax1.hist(muster, bins=bins, color=color, alpha=0.4, label=label)
    return ax1

def show_kdf_his(ax1,ax2,subTitle):
    ax1.legend()
    ax2.legend()
    sns.set_theme(style="whitegrid")
    loc = plticker.MultipleLocator(base=.5) # this locator puts ticks at regular intervals
    ax2.xaxis.set_major_locator(loc)
    #plt.style.use('ggplot')
    plt.style.use('seaborn-whitegrid')
    plt.savefig(Configurations.plotPath+"_"+subTitle+'.png',dpi=300)
    plt.show()
    plt.close()

def show_his(ax1,subTitle):
    ax1.legend()
    sns.set_theme(style="whitegrid")
    loc = plticker.MultipleLocator(base=.5) # this locator puts ticks at regular intervals
    ax1.xaxis.set_major_locator(loc)
    #plt.style.use('ggplot')
    #plt.style.use('seaborn-whitegrid')
    plt.savefig(Configurations.plotPath+"_hist_"+subTitle+'.png',dpi=300)
    plt.show()
    plt.close()

def plot3kd(natural,trendup,trenddown,label_1,label_2,label_3,subTitle):
    fig, (ax1, ax2) = getHis_axs()
    bins =7
    ax1,ax2=add_kde(ax1,ax2,natural,label_1,'g',bins)
    ax1,ax2=add_kde(ax1,ax2,trendup,label_2,'r',bins)
    ax1,ax2=add_kde(ax1,ax2,trenddown,label_3,'b',bins)
    show_kdf_his(ax1,ax2,subTitle)
    return ax1,ax2

def plotHistogram_5(kd_1,kd_2,kd_3,kd_4,kd_5,label_1,label_2,label_3,label_4,label_5,subTitle):
    fig, ax1 = plt.subplots(nrows=1, sharex=True,figsize=(10, 5))
    bins =10
    ax1=add_his(ax1,kd_1,label_1,'g',bins)
    ax1=add_his(ax1,kd_2,label_2,'r',bins)
    ax1=add_his(ax1,kd_3,label_3,'b',bins)
    ax1=add_his(ax1,kd_4,label_4,'m',bins)
    ax1=add_his(ax1,kd_5,label_5,'c',bins)
    show_his(ax1,subTitle)
    return ax1

def plot5kd(kd_1,kd_2,kd_3,kd_4,kd_5,label_1,label_2,label_3,label_4,label_5,subTitle):
    fig, (ax1, ax2) = getHis_axs()
    bins =10
    ax1,ax2=add_kde(ax1,ax2,kd_1,label_1,'g',bins)
    ax1,ax2=add_kde(ax1,ax2,kd_2,label_2,'r',bins)
    ax1,ax2=add_kde(ax1,ax2,kd_3,label_3,'b',bins)
    ax1,ax2=add_kde(ax1,ax2,kd_4,label_4,'m',bins)
    ax1,ax2=add_kde(ax1,ax2,kd_5,label_5,'c',bins)
    show_kdf_his(ax1,ax2,subTitle)
    return ax1,ax2

def plotSlopes(subtitle):
    slopes =[]
    df = statisticsUtils.getLinRegParaDataFrame(Configurations.trendUptrainDatasetName,Configurations.targetColumName_trendUp,0.0)
    x = df.slops.values
    slopes.append(x)
    plotMetricHistogram(x,"No Trend and Shift",subtitle)

    df =statisticsUtils.getLinRegParaDataFrame(Configurations.trendUptrainDatasetName,Configurations.targetColumName_trendUp,1.0)
    x = df.slops.values
    slopes.append(x)
    plotMetricHistogram(x,"Trend up",subtitle)

    df = statisticsUtils.getLinRegParaDataFrame(Configurations.trendDowntrainDatasetName,Configurations.targetColumName_trendDown,1.0)
    x = df.slops.values
    slopes.append(x)
    plotMetricHistogram(x,"Trend down",subtitle)

    df = statisticsUtils.getLinRegParaDataFrame(Configurations.shiftUptrainDatasetName,Configurations.targetColumName_shiftUp,1.0)
    x = df.slops.values
    slopes.append(x)
    plotMetricHistogram(x,"Shift up",subtitle)

    df = statisticsUtils.getLinRegParaDataFrame(Configurations.shiftDowntrainDatasetName,Configurations.targetColumName_shiftDown,1.0)
    x = df.slops.values
    slopes.append(x)
    plotMetricHistogram(x,"Shift down",subtitle)
    return slopes

def plotLoss(history):
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig('../plots/{}'.format("plotLoss"), dpi=300)
    plt.show()

def plotAccuracy(history):
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig('../plots/{}'.format("plotAccuracy"), dpi=300)
    plt.show()

# def plotAccuracyToLayersNumber(accuracy,layersNumber):
#     plt.plot(accuracy,layersNumber)
#     plt.title('model accuracy to number of layers ')
#     plt.ylabel('number of layers')
#     plt.xlabel('accuracy')
#     plt.savefig('../plots/{}'.format("plotAccuracyToLayersNumber"), dpi=300)
#     plt.show()