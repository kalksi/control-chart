import os
from statistics import mean
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("Configurations"))))
import Configurations
from utils import statisticsUtils

#tolerance = Configurations.shift_tolerance
mean = 0
numberOfPointsInARow = Configurations.numberOfPointsInARow_shift

def validPointsInControlLimtes(K_u, K_o, points):
    result = False
    sumOfValidPoints = 0
    for point in points:
        if(point > K_u) and (point < K_o):
            sumOfValidPoints = sumOfValidPoints+1
    if(sumOfValidPoints == len(points)):
        result = True
    return result


def checkShiftUpCondition(point, mean):
    return True if (point > mean + Configurations.shift_tolerance) else False


def checkShiftDownCondition(point, mean):
    return True if (point < mean - Configurations.shift_tolerance) else False


def checkNotTrend(chart, min, max):
    slope, intercept, mae, mse = statisticsUtils.getLinRegParam(chart)
    if((slope[0] > min) and (slope[0] < max)):
        return True
    else:
        return False

def checkSlopeInThreshold(direction,chart):
    slope, intercept, mae, mse = statisticsUtils.getLinRegParam(chart)
    if direction == 1:
        if(slope[0] > Configurations.slope_trend_up_min) :
         return True
        else:
         return False
    else:
        if(slope[0] < Configurations.slope_trend_Down_max) :
          return True
        else:
          return False

def checkShift(mean, points, numberOfPointsInARow, shiftFaktor):
    isShiftUp = False
    startIndex = 0
    matches = 0
    while ((startIndex < len(points))):
        for i in range(startIndex, len(points)):
            checkResult = checkShiftUpCondition(points[i], mean) if(
                shiftFaktor > 0) else checkShiftDownCondition(points[i], mean)
            if (checkResult):
                matches = matches+1
                if (matches == numberOfPointsInARow):
                    isShiftUp = True
                    break
            else:
                matches = 0
                break
        startIndex = i+1
    return isShiftUp

def isShiftUp(chart):
    return checkShift(mean, chart, numberOfPointsInARow, 1)

def isShiftDown(chart):
    return checkShift(mean, chart, numberOfPointsInARow, -1)