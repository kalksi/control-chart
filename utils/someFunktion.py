
def simulateShift(meanList,n):
    shiftSimulation =[]
    for i in range(n):
     shiftSimulation.append(simulateOneShiftChart(meanList))
    return shiftSimulation

def simulateNaturalChart(mean,n):
    meanList = [mean]*n
    naturalSimulation =[]
    for i in range(n):
        chart=[]
        stop = False
        while(not stop):
            chart=simulaltionUtils.samplesGenerator(meanList,setting.standardDeviation)
            shift = check.checkShift_up(0,chart,setting.numberOfPointsInARow)
            notTrend = check.checkNotTrend(chart,-0.05,0.05)
            stop = (not shift) and (notTrend)

        naturalSimulation.append(chart)
    return naturalSimulation