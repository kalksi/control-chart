import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
from utils import chartChecker as check
from utils import dataframeUtil as dataUtil
import Configurations
import pandas as pd
import numpy as np
import statistics
import math

# Global Configuration
#slopeFilterNatural =Configurations.natural_optimize_simulation_with_given_slope
#numberOfPointsInARow_shift= Configurations.numberOfPointsInARow_shift
#number_of_subgroups = Configurations.n
#mean = Configurations.mean
#standardDeviation = Configurations.standardDeviation

# Simulation Methods
def getNormalDistribution(mean, standardDeviation,size):
    return np.random.normal(loc=mean, scale=standardDeviation,
                            size=size)

def samplesGenerator(meansList, standardDeviation):
    samples = []
    number_of_subgroups = len(meansList)
    mean = Configurations.mean
    for i in range(number_of_subgroups):
        normal = getNormalDistribution(meansList[i], standardDeviation,number_of_subgroups)
        x =standraizer(normal,mean,number_of_subgroups)
        samples.append(x)
    return samples

def standraizer(normalDistribution,mean,size):
    standardized = ((statistics.mean(normalDistribution)-mean) /
             Configurations.sigma) * math.sqrt(size)

    return standardized

def getOneChart(meansList,standardDeviation):
    x,y = [],[]
    pointsDic = {
        "x": [[]],
        "y": []
    }
    generatedSamples = samplesGenerator(
            meansList, standardDeviation)
    x.append(generatedSamples)
    y.append(1)
    pointsDic["x"] = x
    pointsDic["y"] = y

    return pointsDic

# Trend and slow Changes
def getTrendMeanList(direction,trendStartPoint,trendEndPoint,timeSeriesSize,slope):
    meansList = []
    x = 1  
    for mean_index in range(timeSeriesSize):
        if(mean_index >= trendStartPoint and mean_index < trendEndPoint):
            meansList.append(direction*x*slope + Configurations.mean )
            x = x + 1
        else:
            meansList.append(Configurations.mean)
    return meansList

def getTrendQuadraticMeanList(direction,trendStartPoint,trendEndPoint,timeSeriesSize,slope):
    meansList = []
    x = 1  
    for mean_index in range(timeSeriesSize):
        if(mean_index >= trendStartPoint and mean_index < trendEndPoint):
            meansList.append(direction*x*slope + Configurations.mean )
            x = x + 1
        else:
            meansList.append(Configurations.mean)
    return meansList

def simulateLinearTrend(direction,trendStartPoint,trendEndPoint,timeSeriesSize,slope,optimize_simulation_with_given_slope):
    meansList = getTrendMeanList(direction,trendStartPoint,trendEndPoint,timeSeriesSize,slope)
    pointsDic = simulateTrend(direction,optimize_simulation_with_given_slope,timeSeriesSize,meansList)
    return pointsDic

def simulateQuadraticTrend(direction,trendStartPoint,trendEndPoint,timeSeriesSize,slope,optimize_simulation_with_given_slope):
    meansList = getTrendQuadraticMeanList(direction,trendStartPoint,trendEndPoint,timeSeriesSize,slope)
    pointsDic = simulateTrend(direction,optimize_simulation_with_given_slope,timeSeriesSize,meansList)
    return pointsDic

def simulateTrend(direction,optimize_simulation_with_given_slope,timeSeriesSize,meansList):
    foundTrue = 0
    foundFalse = 0
    x = []
    y = []
    pointsDic = {
        "x": [[]],
        "y": []
    }
    
    #generate charts with Trend 
    while (foundTrue < Configurations.generatedTrainDataOutputNumber_True):
        isValid = True
        chart = samplesGenerator(meansList, Configurations.standardDeviation)
        if(optimize_simulation_with_given_slope):
            isValid = check.checkSlopeInThreshold(direction,chart[0:Configurations.numberOfPointsInARow_trend])
        if(isValid):
            x.append(chart)
            y.append(1)
            foundTrue = foundTrue+1
            pointsDic["x"] = x
            pointsDic["y"] = y         
    #generate charts without Trend
    while (foundFalse != Configurations.generatedTrainDataOutputNumber_False):
        chart=simulateOneNaturalChart(Configurations.mean,timeSeriesSize)
        x.append(chart)
        y.append(0)
        foundFalse = foundFalse+1
    pointsDic["x"] = x
    pointsDic["y"] = y
    return pointsDic

###################


def getStandardizedNormalDistribution():
     meansList = [Configurations.mean] * Configurations.neuralInputsNumber
     return samplesGenerator(meansList, Configurations.standardDeviation)

# - Natural Simulation Methos
def simulateOneNaturalChart(mean,n):
    meanList = [mean]*n
    chart=[]
    stop = False
    tmp = Configurations.shift_tolerance
    Configurations.shift_tolerance = 0
    while(not stop):
        chart=samplesGenerator(meanList,Configurations.standardDeviation)
        shiftup = check.checkShift(0,chart,Configurations.numberOfPointsInARow_shift,1)
        shiftdown = check.checkShift(0,chart,Configurations.numberOfPointsInARow_shift,-1)
        if(Configurations.natural_optimize_simulation_with_given_slope):
            notTrend = check.checkNotTrend(chart,Configurations.slope_no_trend_min,Configurations.slope_no_trend_max)
            stop = (not shiftup) and (not shiftdown) and(notTrend)
        else:
            stop = (not shiftup) and (not shiftdown)
        #plotUtils.plotChart(chart,"Regelkarte")
    Configurations.shift_tolerance = tmp
    return chart

def simulateNatural(number_of_simulations,name):
    found_natural = 0
    x = []
    y = []
    trainData = {
        "x": [[]],
        "y": []
    }
    while (found_natural != number_of_simulations):
        chart=simulateOneNaturalChart(Configurations.mean,Configurations.n)
        x.append(chart)
        y.append(1)
        found_natural = found_natural+1
    trainData["x"] = x
    trainData["y"] = y
    df = dataUtil.setUpDataFrame(Configurations.neuralInputsNumber,Configurations.targetColumName_natural)
    df = dataUtil.fillDataFrame(df, trainData)
    dataUtil.exportDataFrame(df, name)
    
def simulateNaturalTrainAndTestData():
    if(Configurations.simulateNaturalcharts):
        number_of_simulations =Configurations.natural_traindata_number
        simulateNatural(number_of_simulations,Configurations.natural_trainDatasetName)
        simulateNatural(number_of_simulations,Configurations.natural_testDatasetName)
        print("Natural Charts Simulation is done..")
    else:
        print("Natural Charts is disabled from Cofiguraions..")

# - Demo Methods
def getTimeSeries(normalDistribution, size):
    date = pd.date_range('01.10.2022 00:00:00', periods=size,freq="S")
    timeSeriesDict = {
        #"Timestamp":pd.date_range('01.10.2022 00:00:00', periods=50,freq="S"),
        "Sensor": normalDistribution
    }
    df = pd.DataFrame(timeSeriesDict)
    df = df.set_index(pd.DatetimeIndex(date))
    return df

def getNormalDistributedTimeSeries(timeSeriesSize):
    timeSeries = []
    for _ in range(timeSeriesSize):
        normalDistribution = getNormalDistribution(Configurations.mean, Configurations.standardDeviation,Configurations.n)
        for el in normalDistribution:
         timeSeries.append(el)
    return timeSeries

# to delete

# def getInControlChart():
#     result = False
#     #meansList = [Configurations.mean] * Configurations.neuralInputsNumber
#     while (result != True):
#       #generatedSamples = generatorUtils.samplesGenerator(meansList,Configurations.standardDeviation)
#       chart = getNormalizedNormalDistributionTimeSeries().tolist()
#       result = check.validPointsInControlLimtes(
#                 Configurations.K_u, Configurations.K_o, chart)    
#     return chart

# def shiftUpPredictionInterpreter(prediction):
#     detection = ""
#     if(np.round(prediction)[0][0] == 0):
#         detection = "shift up"
#     else:
#         detection = "No shift up"
#     return detection

# def getNormalizedNormalDistributionTimeSeries():
#     timeSeriesSize = Configurations.neuralInputsNumber
#     normalDistribution = getNormalDistributedTimeSeries(timeSeriesSize)
#     df = getTimeSeries(normalDistribution, len(normalDistribution))
#     resample = df["Sensor"].resample(str(timeSeriesSize)+'S').mean()
#     normalized_df=(resample-resample.mean())/resample.std()
#     return normalized_df
def testParam():
    print("Configurations.natural_trainDatasetName")
    print(Configurations.natural_trainDatasetName)
    print("Configurations.natural_testDatasetName")
    print(Configurations.natural_testDatasetName)
