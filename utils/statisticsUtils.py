
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
from utils import chartChecker as check
import Configurations
import pandas as pd
import numpy as np
import statistics
import math
from sklearn.linear_model import LinearRegression
from sklearn import metrics



def getMeanChanges(TimeSeries,mean):
    meanCahnges = []
    meanCahnges.append(mean)
    #meanCahnges.append(statistics.mean(TimeSeries))
    for i in range(1,len((TimeSeries))):
        currentMean = statistics.mean(TimeSeries[0:i])
        #print(currentMean)
        meanCahnges.append(currentMean)

    return meanCahnges

def squaresTotal(TimeSeries,mean):
    squareTotal = []
    squareTotal.append(mean)
    for i in range(1,len((TimeSeries))):
        currentSST= (TimeSeries[i]-mean)**2
        squareTotal.append(currentSST)
    return squareTotal


def sumOfSquaresTotal(squareTotal):
    sst = []
    for i in range(1,len((squareTotal))):
        currentSST= sum(squareTotal[0:i])
        sst.append(currentSST)
    return sst
    
def Average(lst):
    return sum(lst) / len(lst)


def getLinRegParam(y):
    x = np.arange(0, len(y), 1, float).reshape(-1, 1)
    lm = LinearRegression()
    lm.fit(x, y)
    y_pred = lm.predict(x)
    mae = metrics.mean_absolute_error(y,y_pred)
    mse = metrics.mean_squared_error(y,y_pred)
    #accuracyScore=metrics.accuracy_score(y,y_pred)
    return  lm.coef_,  lm.intercept_ , mae , mse

def getSlope(x):
    slope,  _ , _ , _=getLinRegParam(x)
    return slope[0]

def getNormalDistParam(x):
    means =[]
    stds = []
    for set in x:
        means.append(np.mean(set))
        stds.append(np.std(set))
    return np.asanyarray(means) ,np.asanyarray(stds) 

def getLinRegParaDataFrame(dataPath,TargetVarName,TargetVarValue):
    slops=[]
    intercepts=[]
    MeanAbsoluteError=[]
    MeanSquaredError=[]
    points=[]
    #accuracyScore=[]
    df = pd.read_csv(dataPath)
            
    chart = df.loc[df[TargetVarName] == TargetVarValue]
    for i in range(len(chart)-1):
        row = chart.iloc[i:i+1, :-1].values
        slope, intercept,mae , mse  = getLinRegParam(row[0])
        slops.append(slope[0])
        intercepts.append(intercept)
        MeanAbsoluteError.append(mae)
        MeanSquaredError.append(mse)
        points.append(row[0])
        #accuracyScore.append(accuracyScore)
    
    dfDic= {
        "slops":slops,
        "intercepts":intercepts,
        "MeanAbsoluteError":MeanAbsoluteError,
        "MeanSquaredError":MeanSquaredError,
        "points":points
        #"accuracyScore":accuracyScore
    }
    #print(slops,intercepts)
    return  pd.DataFrame(dfDic)

def addReqSlope(x):
    x_with_slope=[]
    x = x.tolist()
    for i in range(len(x)):
        tmp = []
        #tmp.append(list(x[i]))
        slope, intercept,mae , mse = getLinRegParam(x[i])
        l= x[i]
        l.append(slope[0])
        x_with_slope.append(l)
    return np.array(x_with_slope)

def getRegSlopesForAllPattern(dir):
    slopes =[]
    df = getLinRegParaDataFrame(dir+Configurations.trendUptrainDatasetName,Configurations.targetColumName_trendUp,0.0)
    x = df.slops.values
    slopes.append(x)
    
    df =getLinRegParaDataFrame(dir+Configurations.trendUptrainDatasetName,Configurations.targetColumName_trendUp,1.0)
    x = df.slops.values
    slopes.append(x)
 
    df = getLinRegParaDataFrame(dir+Configurations.trendDowntrainDatasetName,Configurations.targetColumName_trendDown,1.0)
    x = df.slops.values
    slopes.append(x)

    df = getLinRegParaDataFrame(dir+Configurations.shiftUptrainDatasetName,Configurations.targetColumName_shiftUp,1.0)
    x = df.slops.values
    slopes.append(x)
 
    df = getLinRegParaDataFrame(dir+Configurations.shiftDowntrainDatasetName,Configurations.targetColumName_shiftDown,1.0)
    x = df.slops.values
    slopes.append(x)
   
    return slopes