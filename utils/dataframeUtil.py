from random import randrange
import pandas as pd
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("utils"))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("Configurations"))))
import Configurations as configs
from utils import plotUtils
import numpy as np

plotUtils.figsize = configs.figSize
#neuralInputsNumber=configs.neuralInputsNumber

# create an empty dataframe to add rows to it
def setUpDataFrame(numberOfIndependentVariables, simulationName):
    TableHeaders = []
    for columnNumber in range(numberOfIndependentVariables):
        TableHeaders.append('x'+str(columnNumber))
    TableHeaders.append(simulationName)
    df = pd.DataFrame(columns=TableHeaders)
    return df

def fillDataFrame(df, trainData):
    for i in range(len(trainData["y"])):
        list = trainData["x"][i][0:configs.neuralInputsNumber]
        list.append(trainData["y"][i])
        df.loc[i] = list
    return df

def exportDataFrame(df, fileName):
    print("exporting to:",fileName)
    df.to_csv(fileName, index=False)
    # df.to_excel(fileName, index=False)

def getXColumnNames():
    columnsNames = []
    for column in range(configs.neuralInputsNumber):
        columnsNames.append('x'+str(column))
    return columnsNames

def getRandomChartFrom_csv(path):
    #print("Reading Data form",path,"Number of x is",str(configs.neuralInputsNumber))
    df = pd.read_csv(path)
    a = randrange(0 , len(df))
    b = a+1
    x = df.iloc[a:b, 0:configs.neuralInputsNumber]
    y = df.iloc[a:b, configs.neuralInputsNumber:,]
    return x,y

def getNaturalDataFrame():
    df_train = pd.read_csv(configs.natural_trainDatasetName)
    df_test = pd.read_csv(configs.natural_testDatasetName)
    return df_train,df_test

def getTargetVarNames():
    targetVarNames = [ configs.targetColumName_trendUp,
                                configs.targetColumName_trendDown, configs.targetColumName_shiftUp, configs.targetColumName_shiftDown]
    if(configs.quadraticTrendMode):
        targetVarNames.append(configs.targetColumName_quadraticTrendUp)
        targetVarNames.append(configs.targetColumName_quadraticTrendDown)
    if(configs.simulateNaturalcharts):
        targetVarNames.append(configs.targetColumName_natural)
    return targetVarNames

def concatenateTowCSV(tableOneLocation, tableTwoLocation, targetVarFirstTableName, targetVarSecondTableName):
    pd1 = pd.read_csv(tableOneLocation)
    pd2 = pd.read_csv(tableTwoLocation)
    train_df = pd1.append(pd2, ignore_index=True)
    train_df[targetVarFirstTableName] = train_df[targetVarFirstTableName].fillna(0)
    train_df[targetVarSecondTableName] = train_df[targetVarSecondTableName].fillna(0)
    train_df = train_df.sample(frac=1)

    return train_df


def concatenateTowDF(pd1, pd2):
    df = pd1.append(pd2, ignore_index=True)
    return df.sample(frac=1)

def concatenateAllData():
    #join uo and down shift and trend
    train_trend_df, test_trend_df = join_trend_up_and_down()
    train_shift_df, test_shift_df = join_shift_up_and_down()
    #join shift and trend
    train_df = concatenateTowDF(train_trend_df, train_shift_df)
    test_df = concatenateTowDF(test_trend_df, test_shift_df)
    #join quadraticTrendMode
    print("quadraticTrendMode is ",configs.quadraticTrendMode)
    if(configs.quadraticTrendMode):
     train_quadraticTrend_df, test_quadraticTrend_df = join_quadraticTrend_up_and_down()
     train_df = concatenateTowDF(train_df, train_quadraticTrend_df)
     test_df = concatenateTowDF(test_df, test_quadraticTrend_df)
     #train_df[[configs.targetColumName_quadraticTrendUp,configs.targetColumName_quadraticTrendDown]] = train_df[[configs.targetColumName_quadraticTrendUp,configs.targetColumName_quadraticTrendDown]].fillna(0)
     #test_df[[configs.targetColumName_quadraticTrendUp,configs.targetColumName_quadraticTrendDown]] = test_df[[configs.targetColumName_quadraticTrendUp,configs.targetColumName_quadraticTrendDown]].fillna(0)
    train_df.head()
    #join natural
    if(configs.simulateNaturalcharts):
       natural_df_train,natural_df_test = getNaturalDataFrame()
       train_df = concatenateTowDF(train_df, natural_df_train)
       test_df = concatenateTowDF(test_df, natural_df_test)  

    train_df[getTargetVarNames()] = train_df[getTargetVarNames()].fillna(0)
    test_df[getTargetVarNames()] = test_df[getTargetVarNames()].fillna(0) 
    exportDataFrame(train_df.sample(frac=1), configs.trainDatasetName if(not configs.quadraticTrendMode) else configs.trainDatasetNameQuadratic )
    exportDataFrame(test_df.sample(frac=1), configs.testDatasetName if(not configs.quadraticTrendMode) else configs.testDatasetNameQuadratic)
    return train_df,test_df

def join_quadraticTrend_up_and_down():
    #join_quadraticTrend_up_and_down
    train_quadraticTrend_df = concatenateTowCSV(
        configs.quadraticTrendUptrainDatasetName,
        configs.quadraticTrendDowntrainDatasetName, 
        configs.targetColumName_quadraticTrendUp,
        configs.targetColumName_quadraticTrendDown)
    test_quadraticTrend_df = concatenateTowCSV(
        configs.quadraticTrendUptestDatasetName,
        configs.quadraticTrendDowntestDatasetName,
        configs.targetColumName_quadraticTrendUp,
        configs.targetColumName_quadraticTrendDown)
        
    return train_quadraticTrend_df,test_quadraticTrend_df

def join_shift_up_and_down():
    #join_shift_up_and_down
    train_shift_df = concatenateTowCSV(
        configs.shiftUptrainDatasetName,
        configs.shiftDowntrainDatasetName, 
        configs.targetColumName_shiftUp,
        configs.targetColumName_shiftDown)
    test_shift_df = concatenateTowCSV(
        configs.shiftUptestDatasetName,
        configs.shiftDowntestDatasetName,
        configs.targetColumName_shiftUp,
        configs.targetColumName_shiftDown)
        
    return train_shift_df,test_shift_df

def join_trend_up_and_down():
    #join trend up and down
    train_trend_df = concatenateTowCSV(
        configs.trendUptrainDatasetName,
        configs.trendDowntrainDatasetName,
        configs.targetColumName_trendUp,
        configs.targetColumName_trendDown)
    test_trend_df = concatenateTowCSV(
        configs.trendUptestDatasetName,
        configs.trendDowntestDatasetName,
        configs.targetColumName_trendUp,
        configs.targetColumName_trendDown)
        
    return train_trend_df,test_trend_df

def getTrainAndTestData(pathTraindata,pathTestdata,targetVarNumber,neuralInputsNumber):
    train_df = pd.read_csv(pathTraindata)
    test_df = pd.read_csv(pathTestdata)
    np.random.shuffle(train_df.values)
    x = train_df.iloc[:,0:neuralInputsNumber].values
    y = train_df.iloc[:,neuralInputsNumber:neuralInputsNumber+targetVarNumber].values
    test_x = test_df.iloc[:,0:neuralInputsNumber].values
    test_y = test_df.iloc[:,neuralInputsNumber:neuralInputsNumber+targetVarNumber].values
    train_df.head()
    return x,y,test_x,test_y

def getDataset(path_dataset,numberOf_x,numberOf_y):
    train_df = pd.read_csv(path_dataset)
    x = train_df.iloc[:,0:numberOf_x].values
    y = train_df.iloc[:,numberOf_x:numberOf_x+numberOf_y].values
    train_df.head()
    return x,y
